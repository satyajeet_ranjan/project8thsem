import urllib as ul   #import for accesing urls
import os   #for accesing os features like launching an app
import time as tim   #time library
import winsound as playsound   #winsound for playing sound

last_time = 0 #time variable to check how long the problem has been happening
browser_opened = False     #boolean to check if the browser has been opened or not

#main class for the checking of data
class Main:
    date = ""
    time = ""
    latitude = ""
    longitude = ""
    heartRate = ""
    problem = ""
    def assign_values(self,d,t,la,lo,p,hr):   #function to assign values to class variables
        self.heartRate = hr
        self.problem = int(p)
        self.longitude = lo
        self.latitude = la
        self.date = d
        self.time = t
        self.check_data()   #calling check_data function to further processing of data

    def check_data(self):
        global last_time, browser_opened
        if self.problem == 1:   #if user is in problem
            if tim.time()-last_time > 30:     #if the browser last open time was 30 seconds before
                time = tim.time()
                if not browser_opened:
                    os.startfile("https://project8thsem.000webhostapp.com")
                    browser_opened = True
                last_time = time

            else:
                # print tim.time()-last_time
                pass

            playsound.Beep(960, 800)   #beep sounds
            playsound.Beep(770, 800)
        else:
            last_time = 0
            browser_opened = False


obj = Main()  #creating objects for Main class

#running infinite loop to check for data from server regularily
while True:
    try:

        f = ul.urlopen("http://project8thsem.000webhostapp.com/raw.php") #opening the url and storing in f
        response = f.readlines()  #reading data from f and saving in response
        response = response[0].split(",")  #the data comes in csv format split it on the  coma
        #assigning the data from server to variables
        date = response[0]
        time = response[1]
        heartRate = response[2]
        latitude = response[3]
        longitude = response[4]
        problem = response[5]
        #assign the variables in class using the class object
        obj.assign_values(date, time, latitude, longitude, problem, heartRate)

    except Exception as ex:
        print ex.args
